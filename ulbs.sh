#!/bin/sh
# ULBS: Universal Linux Bootstrap Installer
## TODO:
# check for fonts
# drivers (nvidia)
# configure git from script
# Check for Tg
# https://telegram.org/dl/desktop/linux
# kindlegen file ( use scp with debian server)
# check for all return 1 lines, it may be that there is nowhere to return
# resulting in hang up
# after yay installs it hangs, check it
# add emoji font

LOCAL_DFILES_DIR="./dfiles"
LOCAL_DFILES_SCRIPT="./dfiles.sh"
PKGS="vim fish bspwm polybar sxhkd fzf ranger shellcheck syncthing viewnior\
virtualbox thunderbird chromium pcmanfm dmenu pywal feh jq"
AUR_PKGS="kcc kindlegen hakuneko-desktop diskonaut"

# Throw error messages
error () {
    read -r type ARGS <<_END_
$@
_END_
    TYPE=$(echo "$type" | sed 's/-i/install/; s/-c/clone/; s/-d/download/')
    printf "%s ERROR: Failed to %s %s.\n"\
        "$(date '+%H:%M %d/%m/%y')" "$TYPE" "$ARGS" | tee -a ulbs-err_log.txt
}

# See if git is installed
check_for_git () {
    if ! command -v git > /dev/null 2>&1; then
        printf "git is not installed, trying to install git..."
        sudo pacman -S git || error -i "git" && return 1
    fi
}

# Deploy dotfiles
deploy_dotfiles () {
    DFILES_REPO="https://www.gitlab.com/Andreias/dfiles.git"
    if [ -f "$LOCAL_DFILES_SCRIPT" ] && [ -d "$LOCAL_DFILES_DIR" ]; then
        printf "Using local repository to deploy dot files...\n"
        ./dfiles | tee ulbs-dfiles_log.txt
    else
        check_for_git || return 1
        printf "Couldn't find local repo, proceeding to clone it...\n"
        git clone || error -c "git"
    fi
}

# Install yay
install_yay () {
    printf "Installing yay...\nInstalling 'git' and 'base-devel'\n"
    if ! sudo pacman -S --needed git base-devel; then
        error -i "git and base-devel" && exit 1
    else
        if git clone https://aur.archlinux.org/yay.git; then
            cd yay && makepkg -si
        else
            error -c "clone yay" && exit 1
        fi
    fi
}

# Install vim plugins with vim-plug
vim_install_plugins () {
    VIM_RC_FILE="$HOME/.vimrc"
    if [ ! -f "$VIM_RC_FILE" ]; then
        printf "%s not found, aborting...\n" "$VIM_RC_FILE" && return 1
    fi

    if ! grep -q "call plug#begin" "$VIM_RC_FILE"; then
        printf "No configuration for vim-plug found.\n"
    else
        printf "Installing plugins...\n"
        vim +PlugInstall +qall
    fi
}

# Install vim-plug
vim_install_plug () {
    URL="https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
    PLUG_VIM="$HOME/.vim/autoload/plug.vim"
    if curl -fLo "$PLUG_VIM" --create-dirs "$URL" > /dev/null 2>&1; then
        printf "vim-plug cloned successfully.\n"
        vim_install_plugins
    else
        error -d "vim-plug"
    fi
}

# Set default shell for sxhkd to sh
set_sxhkd () {
    [ ! -f ".$HOME/.xinitrc" ] && echo "#!/bin/bash" > .xinitrc
    echo "export SXHKD_SHELL='/usr/bin/sh'" >> .xinitrc
}

add_fonts () {
    # curl file && fc-list?
    echo
}

pacman_pkgs () {
    sudo pacman -S $PKGS && exit
}

aur_pkgs () {
    yay -S $AUR_PKGS && exit
}

printf "ULBS (Universal Linux Bootstrap) - Post Installation script.\n
1) Install yay
2) Install pacman packages
3) Install aur packages with yay
4) Install vim-plug and all the plugins
5) Deploy dot files
6) Set sxhkd default shell to bash
7) Install fonts
8) Exit
"

while true; do
    read -r option
    case "$option" in
        1) install_yay;;
        2) pacman_pkgs;;
        3) aur_pkgs;;
        4) vim_install_plug;;
        5) deploy_dotfiles;;
        6) set_sxhkd;;
        7) add_fonts;;
        8) exit;;
        *) continue;;
    esac
done
